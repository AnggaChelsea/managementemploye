import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmpolyeeRoutingModule } from './empolyee-routing.module';
import { DetailEmployeeComponent } from './list-employee/detail-employee/detail-employee.component';
import { ViewAddEditEmployeComponent } from './list-employee/view-add-edit-employe/view-add-edit-employe.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { SearchPipe } from '../../shared/components/pipe/search.pipe';


@NgModule({
  declarations: [
    DetailEmployeeComponent,
    ViewAddEditEmployeComponent,
    SearchPipe
  ],
  imports: [
    CommonModule,
    EmpolyeeRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class EmpolyeeModule { }
