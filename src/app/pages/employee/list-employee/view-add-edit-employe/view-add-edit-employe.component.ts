import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { EmployeeListService } from '../../../../shared/services/employee-list.service'
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-view-add-edit-employe',
  templateUrl: './view-add-edit-employe.component.html',
  styleUrls: ['./view-add-edit-employe.component.scss'],
})
export class ViewAddEditEmployeComponent implements OnInit {
  regisForm: FormGroup;
  alert = false
  email = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
  unixCode = Math.floor(Math.random() * 100000)
  groups = [
    {
      name: 'BE',
      value: 'be',
    },
    {
      name: 'FE',
      value: 'fe',
    },
    {
      name: 'BA',
      value: 'ba',
    },
    {
      name: 'PM',
      value: 'pm',
    },
    {
      name: 'QA',
      value: 'qa',
    },
    {
      name: 'ADMIN',
      value: 'admin',
    },
    {
      name: 'HR',
      value: 'hr',
    },
    {
      name: 'MARKETING',
      value: 'be',
    },
    {
      name: 'BE',
      value: 'be',
    },
    {
      name: 'BE',
      value: 'be',
    },
  ]
  id: any
  dataDetail: any
  action: any
  data: any
  erroDate = false;
  constructor(
    private fb: FormBuilder,
    private employeService: EmployeeListService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
    this.regisForm = this.fb.group({
      id: [this.unixCode],
      username: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(this.email)]],
      birthDate: ['', Validators.required],
      basicSalary: ['', Validators.required],
      status: ['', Validators.required],
      group: ['', Validators.required],
      description: ['', Validators.required],
    })
  }

  ngOnInit(): void {
    this.action = history.state.value
    this.id = history.state.value?.id
    this.data = this.employeService.getData()
    const dataDetail = this.data.filter((item: any) => item.id === +this.id)
    console.log(dataDetail[0]?.username)
    if (this.action?.type === 'edit' || this.action?.type === 'view') {
      if (dataDetail.length > 0) {
        const dateStr = dataDetail[0].birthDate
        const dateObj = new Date(dateStr)
        const formattedDate = `${
          dateObj.getMonth() + 1
        }/${dateObj.getDate()}/${dateObj.getFullYear()}`
        this.regisForm.get(`username`)?.setValue(dataDetail[0].username)
        this.regisForm.get(`firstName`)?.setValue(dataDetail[0].firstName)
        this.regisForm.get(`lastName`)?.setValue(dataDetail[0].lastName)
        this.regisForm.get(`email`)?.setValue(dataDetail[0].email)
        this.regisForm.get(`birthDate`)?.setValue(formattedDate)
        this.regisForm.get(`basicSalary`)?.setValue(dataDetail[0].basicSalary)
        this.regisForm.get('status')?.setValue(dataDetail[0].status)
        this.regisForm.get('group')?.setValue(dataDetail[0].group)
        this.regisForm.get('description')?.setValue(dataDetail[0].description)
      }
    }
    if (this.action?.type === 'view') {
      if (dataDetail.length > 0) {
        this.regisForm.get('username')?.disable()
        this.regisForm.get('firstName')?.disable()
        this.regisForm.get('lastName')?.disable()
        this.regisForm.get('email')?.disable()
        this.regisForm.get('birthDate')?.disable()
        this.regisForm.get('basicSalary')?.disable()
        this.regisForm.get('status')?.disable()
        this.regisForm.get('group')?.disable()
        this.regisForm.get('description')?.disable()
      }
    }
  }

  doAddEmploye() {
    console.log(this.regisForm.value)
    if (this.action?.type === 'edit') {
      const today = new Date().toLocaleString('en')
      const inputDate = new Date(this.regisForm.value.birthDate).toLocaleString('en')
      if(inputDate > today) {
        this.erroDate = true;
        setTimeout(() => {
          this.erroDate = false;
        }, 4000)
        return;
      }else{
        this.employeService.updated(this.regisForm.value, this.id)
        this.alert = true;
        setTimeout(() => {
          this.alert = false;
        }, 3000)
        return
      }
    }else {
      const today = new Date().toLocaleString('en')
      const inputDate = new Date(this.regisForm.value.birthDate).toLocaleString('en')
      if(inputDate > today) {
        this.erroDate = true;
        setTimeout(() => {
          this.erroDate = false;
        }, 4000)
        return;
      }else{
        this.employeService.pushData(this.regisForm.value)
        this.router.navigate([''])
        console.log(this.regisForm.value)
        return
      }

    }
  }
}
