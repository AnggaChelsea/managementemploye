import { Component, OnInit } from '@angular/core';
import { Employee } from '../../../shared/model/employee.model';
import { Router } from '@angular/router';
import { EmployeeListService } from 'src/app/shared/services/employee-list.service'
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-list-employee',
  templateUrl: './list-employee.component.html',
  styleUrls: ['./list-employee.component.scss']
})
export class ListEmployeeComponent implements OnInit {
  employes:any
  isLogin :any;
  page: number = 1;
  itemsPerPage = 5;
  totalItems: any
  isAsc = false;
  textSearch = ''
  remove = false;
  allPage!: number;
  constructor(private router: Router, private employeService: EmployeeListService) { }

  ngOnInit(): void {
    this.getData(1);
    this.isLogin = localStorage.getItem('isLogin')
    console.log(this.isLogin)
  }

  toDetail(action:any, id?: any){
    const data = {
      type: action,
      id: id
    }
    this.router.navigate(['view-add-edit-employe'], {
      state: {value: data}
    })
  }
  edit(id: any){
    const data = {
      type: 'edit',
      id: id
    }
    this.router.navigate([`view-add-edit-employe`], {
      state: {value: data}
    })
  }

  doSearch(event: any){
  this.employes = this.employes.filter((item: any) => item.username === 'a')
  }
  doSortAscDesc(){
    this.isAsc = !this.isAsc

    if (this.isAsc === false) {
      this.employes.sort((a:any, b:any) => a.username.localeCompare(b.username))
    } if(this.isAsc === true) {
      this.employes.sort((a:any, b:any) => b.username.localeCompare(a.username))
    }
  }

  getData(page?: any){
    this.employes = this.employeService.getData()
    this.totalItems = this.employes.length
    console.log(this.employes.length)

  }

  changePage(changePage: any){
    this.itemsPerPage = +changePage.target?.value
  }

  delete(id: any){
    this.employes.splice(id, 1)
    this.getData(1)
    this.remove = true
    setTimeout(() => {
      this.remove = false
    }, 4000)
  }


}
