import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailEmployeeComponent } from './list-employee/detail-employee/detail-employee.component';
import { ListEmployeeComponent } from './list-employee/list-employee.component';
import { ViewAddEditEmployeComponent } from './list-employee/view-add-edit-employe/view-add-edit-employe.component';

const routes: Routes = [
  {
    path: '',
    component: ListEmployeeComponent
  },
  {
    path: 'detail-employee',
    component: DetailEmployeeComponent
  },
  {
    path: 'view-add-edit-employe',
    component: ViewAddEditEmployeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpolyeeRoutingModule { }
