import { Component, OnInit } from '@angular/core';
import { UserAuth } from '../../shared/model/auth.model';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  isLogin = false;
  formAuth: FormGroup
  email = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
  userAuth: UserAuth[] = [
    new UserAuth(
      'admin@gmail.com',
      'admin'
    )
  ]
  error = false;
  constructor(private router: Router, private fb: FormBuilder) {
    this.formAuth = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(this.email)]],
      password: ['', Validators.required]
    })
  }

  ngOnInit(): void {

  }

  login(){
    if(this.userAuth[0].email === this.formAuth.value.email && this.userAuth[0].password === this.formAuth.value.password){
      localStorage.setItem('isLogin', 'true')
      this.router.navigateByUrl('/')
    }else{
      this.error = true;
      setTimeout(() => {
        this.error = false;
      }, 4000)
      localStorage.setItem('isLogin', 'false')
    }
  }

  switch(){
    this.isLogin = !this.isLogin
  }

}
