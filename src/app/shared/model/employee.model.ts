export class Employee {
    public id?: number
    public username!:  string;
    public firstName?: string;
    public lastName?: string;
    public email?: string;
    public birthDate?: Date;
    public basicSalary?: number;
    public status?: string;
    public group?: string;
    public description?: string

    constructor(
      id: number,
      username: string,
      firstName : string,
      lastName : string,
      email : string,
      birthDate : Date,
      basicSalary : number,
      status : string,
      group : string,
      description : string
      ){
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthDate = birthDate;
        this.basicSalary = basicSalary;
        this.status = status;
        this.group = group;
        this.description = description;
      }


   }
