import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
})
export class NavbarComponent implements OnInit {
  isLogin :any;
  show = false
  constructor(private router:Router) { }

  ngOnInit() {
    this.isLogin = localStorage.getItem('isLogin')
    console.log('isLogin', this.isLogin)
  }

  logout(){
    localStorage.setItem('isLogin', 'false')
    window.location.reload()
    this.router.navigate(['/auth']);
  }
  doshowData(){
    this.show = !this.show
  }

}
