# Simple Manageemploye 
demo : https://simpleemploye.vercel.app/


### Here I'm using hardcode data employe has i was creating at my code so im not using http to get any data because I look into technical test have given to me



This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.2.
because I'm working at that version in my office project


login
```
email : admin@gmail.com
password : admin

```


## 1 - before run the froject
```
// download or install nodejs

    https://nodejs.org/en/

// u can download LTS version

After download nodejs check
node -v
npm -v

if there is no error, u success download it
```
## 2 - download angular cli
```
// download angular via npm

npm install -g @angular/cli

// that will download the angular latest (15.2.0)
```

if u already download node and angular,  checkout folder directory this project
<p>and install project</p>
## npm install 

```
// run npm install in your local for generating module package the package needed this project

npm install

// or if that doesnt work, u can use

npm install --legacy-peer-deps

//to ignore the complict
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
